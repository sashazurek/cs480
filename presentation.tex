\documentclass[x11names]{beamer}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{graphicx}
\title{Wildcat: A MISC CPU Architecture}
\date{Winter 2022}
\author{Sasha Freyja Zurek}
\institute{Northern Michigan University}
\subject{Computer Science}
\usetheme{Berlin}
\definecolor{nmugreen}{RGB}{09,66,46}
\usecolortheme[named=nmugreen]{structure}
\setbeamercolor{titlelike}{bg=white,fg=nmugreen}
\hypersetup{pdfstartview={Fit}}
% remove section numbering from sectionpage
\setbeamertemplate{section page}
{
    \begin{centering}
    \begin{beamercolorbox}[sep=12pt,center]{part title}
    \usebeamerfont{section title}\insertsection\par
    \end{beamercolorbox}
    \end{centering}
}
\setbeamertemplate{subsection page}
{
    \begin{centering}
    \begin{beamercolorbox}[sep=12pt,center]{part title}
    \usebeamerfont{section title}\insertsection:\linebreak\insertsubsection\par
    \end{beamercolorbox}
    \end{centering}
}
\lstset{
  language=Scala,
  backgroundcolor=\color{white},   % choose the background color
  basicstyle=\scriptsize\ttfamily,        % size of fonts used for the code
  breaklines=true,                 % automatic line breaking only at whitespace
  captionpos=b,                    % sets the caption-position to bottom
  showstringspaces=false,
  commentstyle=\color{SpringGreen4},    % comment style
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  keywordstyle=\color{DeepPink3},       % keyword style
  identifierstyle=\color{Blue3}\footnotesize,
  stringstyle=\color{Firebrick4},     % string literal style
  frame=single,
  rulecolor=\color{black},
  frameround=tttt,
}
\begin{document}
\begin{frame}\titlepage\end{frame}

\section{Project Overview}
\subsection{Proposal}
\begin{frame}
  \subsectionpage
\end{frame}
\begin{frame}
  \frametitle{Overview}
  The goal of the project is to design a MISC (Minimal Instruction Set Computer)
  CPU with essential, basic features. The CPU should be able to run simple
  assembly programs, be simulated in software, and also potentially be ran
  on an FPGA for demonstration purposes.
\end{frame}
\begin{frame}
  \frametitle{Technologies}
  \begin{itemize}
    \item The simulator will be built in Scala and make use of the
          Chisel hardware construction langauge library for low-level ``hardware''
    \item Utilizing Chisel's Verilog compatibility, there is the potential
          to run the resulting CPU core using an FPGA should time allow for it.
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Learning Objectives}
  \begin{itemize}
    \item Familiarize myself with the essential concepts of CPU architecutre
    \item Develop an understanding of how to practically use hardware description languages
    \item Develop a better understanding of hardware-side development in general
    \item Familiarize myself with Scala and the broader JVM platform
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Goals}
  % the goals on the proposal
  \begin{itemize}
    \item Design a MISC instruction set
    \item Support arithmetic operations
    \item Support binary operations (including bit shifts)
    \item Support jumps and conditional skips
    \item Have status register to allow for conditionals
    \item Implement the instruction set in a software simulator
    \item Load/store values from ``external memory''
    \item Execute code stored on a ROM
    \item Use unit testing on the module-level to verify components of the core
  \end{itemize}
\end{frame}
\subsection{Outcome}
\begin{frame}
  \subsectionpage
\end{frame}
\begin{frame}[shrink=5]
  \frametitle{What is Wildcat?}
  % general overview of the Wildcat ISA and WildCAT
  \begin{columns}[T]
    \begin{column}{6cm}
      The Wildcat ISA is a 24-bit register-based MISC architecture:
      \begin{itemize}
        \item 22 instructions
        \item 32 24-bit working registers
              \begin{itemize}
                \item One reserved as permanent 0
              \end{itemize}
        \item 49.152kB memory maximum (RAM or ROM)
      \end{itemize}
    \end{column}
    \begin{column}{6cm}
      The Wildcat Conceptual Architectural Template (WildCAT) is a
      proof-of-concept, straightforward implementation of the Wildcat ISA:
      \begin{itemize}
        \item Written in Chisel
        \item 503 lines of code
              \begin{itemize}
                \item 185 for the implementation
                \item 318 for the test code
              \end{itemize}
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}
\section{Wildcat ISA}
\begin{frame}
  \sectionpage
  \centering
  Let's look at the spec document!
\end{frame}
\section{WildCAT}
\subsection{Chisel}
\begin{frame}
  \subsectionpage
\end{frame}
\begin{frame}
  \frametitle{What is Chisel?}
  % high-level implementation details -- chisel
  Chisel is a hardware description lanuage:
  \begin{itemize}
    \item Built on Scala and FIRRTL
          \begin{itemize}
            \item Scala program that constructs a hardware graph (circuit)
            \item FIRRTL optimizes and transforms the generated circuits
          \end{itemize}
    \item Translates to Verilog, ASIC, FPGA
    \item High-level features for low-level design
          \begin{itemize}
            \item Hardware generators
            \item Reusable components, libraries
            \item \textbf{Abstraction}
          \end{itemize}
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{What does Chisel look like?}
  \begin{figure}
    \includegraphics[width=\linewidth]{fir_filter.pdf}
    \caption{A FIR filter with a convolution operation \cite{Chisel}}
  \end{figure}
\end{frame}
\begin{frame}[fragile]
  \frametitle{What does Chisel look like?}
  \begin{lstlisting}[caption=Straightforward FIR filter \cite{Chisel}]
// 3-point moving sum implemented in the style of a FIR filter
class MovingSum3(bitWidth: Int) extends Module {
  val io = IO(new Bundle {
    val in = Input(UInt(bitWidth.W))
    val out = Output(UInt(bitWidth.W))
  })

  val z1 = RegNext(io.in)
  val z2 = RegNext(z1)

  io.out := (io.in * 1.U) + (z1 * 1.U) + (z2 * 1.U)
}  
      \end{lstlisting}
\end{frame}
\begin{frame}[fragile,plain]
  \frametitle{What does Chisel look like?}
  \begin{lstlisting}[caption=Parameterized FIR filter \cite{Chisel}]
// Generalized FIR filter parameterized by the convolution coefficients
class FirFilter(bitWidth: Int, coeffs: Seq[UInt]) extends Module {
  val io = IO(new Bundle {
    val in = Input(UInt(bitWidth.W))
    val out = Output(UInt(bitWidth.W))
  })
  // Create the serial-in, parallel-out shift register
  val zs = Reg(Vec(coeffs.length, UInt(bitWidth.W)))
  zs(0) := io.in
  for (i <- 1 until coeffs.length) {
    zs(i) := zs(i-1)
  }

  // Do the multiplies
  val products = VecInit.tabulate(coeffs.length)(i => zs(i) * coeffs(i))

  // Sum up the products
  io.out := products.reduce(_ + _)
}
      \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
  \frametitle{What does Chisel look like?}
  \begin{lstlisting}[caption=Different uses for the parameterized FIR filter \cite{Chisel}]
// same 3-point moving sum filter as before
val movingSum3Filter = Module(new FirFilter(8, Seq(1.U, 1.U, 1.U)))  

// 1-cycle delay as a FIR filter
val delayFilter = Module(new FirFilter(8, Seq(0.U, 1.U)))  

// 5-point FIR filter with a triangle impulse response
val triangleFilter = Module(new FirFilter(8, Seq(1.U, 2.U, 3.U, 2.U, 1.U)))  
  \end{lstlisting}
\end{frame}
\begin{frame}[fragile,plain]
  \frametitle{How do you test this?}
  % show examples from WildCAT
  \begin{lstlisting}[caption=Fibonacci test from WildCAT]
it should "test fibonacci sequence" in {
test(new WildcatCore()) { c =>
  write_ins(c,0.U,6189.U) // ADDI r1 0d6  
  write_ins(c,1.U,1997.U) // ADDI r30, 0d1
  write_ins(c,2.U,1070.U) // fib: SUBI r1 0x1
  write_ins(c,3.U,32704.U) // ADD r30 r31
  write_ins(c,4.U,30693.U) // MOV r31 r29
  write_ins(c,5.U,32709.U) // MOV r30 r31
  write_ins(c,6.U,31653.U) // MOV r29 r30
  write_ins(c,7.U,2092.U) // EQU r1, r0
  write_ins(c,8.U,19.U) // SKP
  write_ins(c,9.U,2066.U) // JMP fib
  write_ins(c,10.U,941.U) // ADDI r29 0d0
  boot(c)
  c.io.boot.poke(false.B)
  for(ins <- 0 until 49) {
    c.clock.step(1)
  }
  c.io.pc.expect(10.U)
  c.io.src.expect(13.U)
}
  \end{lstlisting}
\end{frame}
\subsection{Show us the code!}
\begin{frame}
  \subsectionpage
\end{frame}
\section{References}
\begin{frame}[allowframebreaks]
  \frametitle<presentation>{References}
  \begin{thebibliography}{10}
    \beamertemplatearticlebibitems
    \bibitem{Chisel}
    Chisel/FIRRTL Developers
    \newblock {\em Chisel/FIRRTL Hardware Compiler Framework}.
    \newblock\url{https://www.chisel-lang.org/}
  \end{thebibliography}
\end{frame}

\end{document}