target = 6
n1 = 1
n2 = 0
res = 0

while target:
    res = n1 + n2
    n2 = n1
    n1 = res
    target -= 1

print(res)
    