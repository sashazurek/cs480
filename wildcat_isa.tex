\documentclass[12pt]{article}
\usepackage{fullpage}
\usepackage{listings}
\usepackage{pifont}
\author{Sasha Freyja Zurek}
\date{Version 1.0.4172022}
\title{Wildcat Basic Instruction Set Manual}

\begin{document}
    \maketitle
    \section{Overview}
        This document describes the supported instructions for the Wildcat CPU core,
        as well as providing a general view on how these instructions are implemented.
        \subsection{Wildcat Instruction Set}
            The Wildcat ISA describes the complete set of functionality for the platform.
            Wildcat is a 24-bit load-store Minimal Instruction Set Computer (MISC) which mimics the
            limitations of the oldest computers. Wildcat is not designed to run modern
            programs -- as an educational experience, it's targeted squarely as a proof of
            concept, and towards applications such as basic maths, and simple embedded
            systems.
        \subsection{Goals}
            These goals are repeated from the proposal document. Not all pertain to the ISA
            itself, but all are included here for reference.
            \begin{itemize}
                \item Design a MISC instruction set
                \item Support arithmetic operations
                \item Support binary operations (including bit shifts)
                \item Support jumps and conditional skips
                \item Have status register to allow for conditionals
                \item Implement the instruction set in a software simulator
                \item Load/store values from "external memory"
                \item Execute code stored on a ROM
                \item Use unit testing on the module-level to verify components of the core
            \end{itemize}
    \section{The Wildcat ISA}
    This section describes the  general model for how a Wildcat-compatible CPU is
    implemented. Some information is also included that pertains specifically to the proof-of-concept
    CPU provided with this documentation.
        \subsection{Implementation Details} 
            \subsubsection{Registers}
            Wildcat will have access to 33 24-bit registers, of which 1 is reserved as the
            program counter, and the rest are working registers. Register x0 shall be reserved
             as a permanent zero, and any instructions that try to modify it will be ignored.
            All other registers are accessible by all instructions.
            \subsubsection{Status Register}
            Wildcat has one status register, cond\_pass. It is unset by default, and is only set
            when an equality comparison instruction returns true.
            \subsubsection{Instruction Formats}
                Instruction formats define the opcode ranges to simplify the hardware design.
                All instructions will have the following formats in machine language: 
                \begin{itemize}
                    \item Register-register: dest 14\ding{213}10, src 9\ding{213}5, opcode 4\ding{213}0
                    \item Register-immediate: imm 23\ding{213}10, src 9\ding{213}5, opcode 4\ding{213}0
                \end{itemize}
                When translating machine code into human-readable assembly, the formats look like this
                (with only two exceptions):
                \begin{itemize}
                    \item Register-register: INS rSRC rDEST
                    \item Register-immediate: INS rSRC 0x?
                \end{itemize}
            \subsubsection{Program and Working Memory (ROM and RAM)}
            Wildcat is able to address up to 16,384 individual memory addresses, for a theoretical size
            limit of 49,152 bytes of memory for both ROM and RAM. The proof-of-concept implementation
            has a 768 byte ROM and 3,072 byte RAM.
            \subsubsection{Writing Programs to Wildcat}
            Wildcat has 4 accessible "inputs" which handle writing to program memory and booting the CPU.
            Each instruction written must provide:
            \begin{itemize}
                \item is\_write flag
                \item Write address (where in ROM to place it)
                \item Write data (the actual instruction)
            \end{itemize}
            When all the instructions are written, a user can send the boot flag to "boot" the CPU,
            starting from memory location 0x0. Within the test suite for the proof-of-concept
            demonstration lies these Scala functions for writing instructions and running them:
            \begin{lstlisting}[frame=single]
def write_ins(c: WildcatCore, addr: UInt, code: UInt) = {
    c.io.is_write.poke(true.B)
    c.io.boot.poke(false.B)
    c.io.write_addr.poke(addr)
    c.io.write_data.poke(code)
    c.clock.step(1)
  }
  def boot(c: WildcatCore) = {
    c.io.is_write.poke(false.B)
    c.io.boot.poke(true.B)
    c.clock.step(1)
  }
            \end{lstlisting}
            \subsubsection{Output from Wildcat}
            The proof-of-concept implementation has a number of outputs which are used by the test
            suite, but are not critical to every implementation of Wildcat. The outputs that the
            proof-of-concept provide are here:
            \begin{itemize}
                \item valid -- was the instruction valid?
                \item inst -- the full instruction at current PC
                \item pc -- the current location in ROM
                \item imm -- the value in dest/imm for the current instruction
                \item src -- the value in src for the current instruction
                \item op -- the opcode for the current instruction
                \item cond\_pass -- current state of cond\_pass
                \item result -- the result of the instruction, set to 127 when not applicable
            \end{itemize}
        \subsection{Instructions}
            As of the current revision, these are all legal instructions for Wildcat and are
            necessary in any implementation.
            \subsubsection{Arithmetic Instructions}
            Arithmetic instructions implement simple arithmetic. Save for ADDI and SUBI, these
            only operate between registers, and will follow the Register-Register format.
            \begin{itemize}
                \item ADD -- Add
                \item ADDI -- Add immediate
                \item SUB -- Subtract
                \item SUBI -- Subtract immediate
                \item MUL -- Multiply
                \item DIV -- Divide
                \item MOD -- Modulo
            \end{itemize}
            \subsubsection{Load and Store Instructions}
                Load/store instructions are designed to move values between working registers
                and working memory.
            \begin{itemize}
                \item MOV -- Move/copy
                    \subitem Register-Register format.
                \item LDI -- Load immediate
                    \subitem Register-immediate format, though src is actually your destination.
                        Due to this, direct loading is limited to the range x0\ding{213}x1F
                \item STO -- Store (into memory)
                    \subitem Register-immediate format -- imm represents the location in memory.
                \item LD -- Load from memory
                    \subitem Register-immediate format -- imm is the location in memory, and src is destination.
            \end{itemize}
            \subsubsection{Boolean Instructions}
                Boolean instructions are designed to do boolean arithmetic between registers.
                These all follow the Register-Register format unless otherwise specified.
            \begin{itemize}
                \item AND
                \item OR
                \item XOR 
                \item NOT
                \item BSL - bit shift left (register-immediate)
                \item BSR - bit shift right (register-immediate)
            \end{itemize}
            \subsubsection{Conditional Instructions}
                Conditionals are designed to be used with the ``Conditional Passed'' register flag.
                This removes the need for separate ``and equal'' instructions, as one can simply use
                either of the equality instructions, followed by the EQU instruction, to achieve the
                same result at the cost of an extra compute cycle. These instrucitons can only be used
                with registers, and as such follow the Register-Register format.
            \begin{itemize}
                \item GRE - greater than
                \item LES - less than
                \item EQU - equals
            \end{itemize}
            \subsubsection{Jumps and Skips}
                Branching behavior should be able to be implemented through use of the proper boolean
                logic and combinations of SKP/JMP instructions. Both instructions will follow the
                Register-Immediate format, although in both cases a good portion of the bits are completely
                useless.
            \begin{itemize}
                \item JMP - Jump unconditionally
                    \subitem Format: JMP 0x????
                \item SKP - Skip if conditional passed
                    \subitem Format: SKP
            \end{itemize}
    \section{Examples of Assembly Code}
        \subsection{Fibonacci Sequence}
            This algorithm works for n\textgreater=2, and determines the n+1 fibonacci number.
            Registers are used thusly:
            \begin{itemize}
                \item r1 -- n
                \item r29 -- result register
                \item r30 -- temp register 1
                \item r31 -- temp register 2
            \end{itemize}
            The following code will have ``13'' in r29 when ran to completion:
            \begin{lstlisting}[frame=single]
                addi r1, 0x6
                addi r30, 0x1 # seed initial fibonacci numbers
                fib: subi r1, 0x1 # decrement target value
                    add r30, r31
                    mov r31, r29
                    mov r30, r31
                    mov r29, r30
                    equ r1, r0 # determine if we've finished
                    skp # skip if conditional passed
                    jmp fib # jump to fib otherwise
            \end{lstlisting}
\end{document}