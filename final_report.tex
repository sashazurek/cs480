\documentclass[12pt]{article}
\usepackage{listings}
\usepackage{hyperref}
\title{Wildcat: A MISC CPU Architecture}
\date{Winter 2022}
\author{Sasha Freyja Zurek}
\linespread{1.3}
\begin{document}
    \begin{titlepage}
        \maketitle
        \thispagestyle{empty}
        \setcounter{page}{0}
    \end{titlepage}
    \section{Introduction}
    % what led me to take on this project
    I had taken an interest in the development of RISC-V, an open-source CPU
    architecture, sometime in the last few years. The idea of a fully-open,
    general-purpose CPU was intriguing to me as a FOSS enthusiast -- running 
    open-source software on open-source hardware certainly has a nice ring to it.
    During a particularly slow week at my internship with Kioxia, with more free
    time than any intern should have, I stumbled upon a short course provided by
    the Linux Foundation: a course that, at its conclusion, would result in the
    student having implemented a RISC-V compliant CPU. It sounded neat, so under
    the justification that I was teaching myself something, I took it.
    
    It was far less enthusing than I would have liked. Many of the finer details were
    abstracted away from the student, leaving very little for them to implement.
    It also used a weird dialect of Verilog, with quirky syntax.
    Still, it was interesting, and I learned a lot about an architecture I was 
    interested in but had never taken the time to dive into. As part of the
    temporary hyperfixation I usually get when diving into new topics, I ended up
    finding Chisel -- a hardware description language embedded within Scala. It looked
    interesting: a hardware design language that can make use of high-level programming
    techniques seemed like an interesting mix, as well as being an incredibly useful
    tool.

    The semester following my internship, I took CS330. I found the class really
    neat -- it's a completely different kind of programming to work that close to
    the hardware, and it was nice to get a feel, however small, about what's going on
    behind the hood when I'm writing in high-level languages like C++ or Python.
    The novelty of the course would proceed to stick out to me, when I later had to
    choose a project.
    
    I didn't spend a lot of time before the 2021-2022 winter break to think about what
    I might want to do for a senior project, honestly. The original idea was to do
    a RISC-V compliant CPU implementation of my own creation within Chisel, but
    after some thinking, I thought I should do something more novel: design my own
    CPU.
    \section{Overview}
    % what is Wildcat?
    Wildcat is a 24-bit, load-store, Minimal Instruction Set Computer (MISC) architecture.
    It breaks from typical MISC architectures by having a register-based instruciton set,
    rather than a stack-based set. With only 22 instrucitons, it's not a complex
    CPU architecture, and is not intended to be a replacement for archiectures like
    x86, ARM, or RISC-V. A more apt comparison would be many of the first digial
    computers, like ILLIAC or the Manchester Baby.

    The code portion of the project is comprised of the Wildcat Conceptual Archiectural
    Template (WildCAT) and its associated test suite. Henceforth, ``Wildcat ISA'' shall
    refer to the Wildcat architecture, while ``WildCAT'' shall refer to the specific
    implementation of the Wildcat ISA provided with this project.
    \section{Design}
    I will not spend this section rehashing the details available in the Wildcat ISA
    spec document -- I feel instead that it would be more pertinent to explain some of
    the design choices I made\footnote{The Wildcat ISA spec document is included
    with the project materials.}.

    The decision to make Wildcat ISA 24-bit was rather arbitrary. I didn't want to
    make it a "normal" 8 or 16 bit design, and 32-bit was far too large for a MISC, so I
    settled on the first even number I felt could support up to 32 registers with
    ``enough'' length for large numbers -- I wanted 32 registers, because I like
    the number 32.

    Other than the working registers, Wildcat has:
    \begin{itemize}
      \item Program Counter register, for moving through ROM
      \item A single status register, for the conditional instructions.
    \end{itemize}
    
    Part of the design ethos for Wildcat was to keep the ISA simple -- some of the
    ideas that played into the design are:
    \begin{itemize}
      \item If an instruction can be easily emulated with a few other instructions,
        don't include it.
      \item Sections of an instruction should handle similarly between instructions.
      \item I can ignore my own guidelines if I like the idea enough.
    \end{itemize}
    These principles will be brought up as necessary while explaining the instructions
    that were implemented.
    \subsection{Instructions}
    This is simply a summary of what was implemented in Wildcat, and not a
    complete technical spec. See the Wildcat ISA spec document for more information
    on the specific implementation details.
    \subsubsection{Arithmetic Instructions}
    \begin{itemize}
        \item ADD -- Add
        \item ADDI -- Add immediate
        \item SUB -- Subtract
        \item SUBI -- Subtract immediate
        \item MUL -- Multiply
        \item DIV -- Divide
        \item MOD -- Modulo
    \end{itemize}
    This category is the largest category of instructions, owing in part to a desire
    to simplify how one would translate "math to do" into "assembly for Wildcat".
    The ADDI and SUBI instructions, while in theory were just arithmetic instructions,
    actually turned out to be the most viable way to load values into Wildcat -- this
    will be expanded on in the next section. 
    \subsubsection{Load and Store Instructions}
    \begin{itemize}
        \item MOV -- Move/copy
        \item LDI -- Load immediate
        \item STO -- Store (into memory)
        \item LD -- Load from memory
    \end{itemize}
    These instructions are the core of Wildcat's memory management. LDI, unfortunately,
    is not the most useful instruction in hindsight. The original thought was to have
    it load some 14-bit value into a register -- turns out, ADDI does that job already,
    and if you need to guarantee emptiness beforehand, a MOV from r0 would do the job.
    The decision was then made to implement LDI as a way to load some 14-bit value
    directly into the first 32 addresses in working memory. As this could be easily
    implemented with an ADDI into an empty register, then a STO, it does break
    one of the main design ideas, and I would probably not have included LDI had I
    figured this out when I finalized which instructions to implement. 
    \subsubsection{Boolean Instructions}
    \begin{itemize}
        \item AND
        \item OR
        \item XOR 
        \item NOT
        \item BSL - bit shift left 
        \item BSR - bit shift right 
    \end{itemize}
    These instructions form the boolean arithmetic that can be done with Wildcat.
    Bit shifts were included simply because I found the idea neat. XOR was included,
    even if it could be emulated with AND, OR, and NOT, just because I also found it
    neat.
    \subsubsection{Conditional Instructions}
    \begin{itemize}
        \item GRE - greater than
        \item LES - less than
        \item EQU - equals
    \end{itemize}
    The conditionals here were the 3 I felt were necessary for a simple ISA design.
    These are intended for use with JMP and SKP\footnote{Discussed in the next section.},
    in order to offer a rudimentary way to implement loops and other constructs.
    Multiple conditionals can be ran in a row to emulate a "greater/less than or equal",
    so specific instructions for those were not deemed necessary.
    \subsubsection{Jumps and Skips}
    \begin{itemize}
        \item JMP - Jump unconditionally
        \item SKP - Skip if conditional passed
    \end{itemize}
    A lot of thought went into figuring out which specific ways of changing the program
    counter should and should not be included. RISC and more complex CPU architectures
    often include different branching instructions, which change the program counter
    when a condition is met. I had trouble figuring out, at least with simple cases
    that the Wildcat ISA was intended to address, any special branching logic that
    couldn't be done with JMP -- an unconditional jump, and SKP -- conditionally skip
    the next instruction. As such, I kept it simple -- JMP and SKP are probably enough
    to do most simple program control logic.
    \subsubsection{Extra Space}
    The Wildcat ISA's 22 instructions require at least 5 bits to represent. Unfortunately,
    this means there are 10 extra, currently unused, opcodes in the Wildcat ISA!
    I didn't want to reduce the amount of available instructions -- I felt, at the time
    the ISA was "made stable" to work on the implementation, each instruction had
    a good enough purpose. Now with the power of hindsight, I'd like to offer some ideas
    for what could have been done with this extra space.
    \begin{itemize}
      \item Give Wildcat an extra way to handle memory, like a stack,
        that can be used alongside the existing registers.
      \item Instructions for interacting with external hardware, and ports
        to connect those hardwares to.
      \item More funky arithmetic instructions.
    \end{itemize}
    The only caveat with pushing Wildcat to include more instructions, would be that
    it'd be hard to argue it's a MISC at all -- 22 is already stretching the meaning of
    "minimal" pretty thin.
    \section{Technologies}
    % what did I use to implement Wildcat?
    The main technology used to implement WildCAT was Chisel -- a hardware description
    language embedded within the Scala language.
    Chisel code, in essence, is just a Scala program which constructs a hardware graph,
    or a digital representation of a hardware circuit.
    From the \href{https://www.chisel-lang.org/}{Chisel website}:
    \begin{quote} 
    Chisel adds hardware construction primitives to the Scala programming language, 
    providing designers with the power of a modern programming language to write complex, 
    parameterizable circuit generators that produce synthesizable Verilog. This generator 
    methodology enables the creation of re-usable components and libraries, such as the 
    FIFO queue and arbiters in the Chisel Standard Library, raising the level of abstraction 
    in design while retaining fine-grained control.
    \end{quote}
    
    As an example of the power that Chisel provides to the user, here's an example
    from the Chisel Bootcamp:
    \begin{lstlisting}[frame=single,basicstyle=\scriptsize]
// Generalized FIR filter parameterized by the convolution coefficients
class FirFilter(bitWidth: Int, coeffs: Seq[UInt]) extends Module {
  val io = IO(new Bundle {
    val in = Input(UInt(bitWidth.W))
    val out = Output(UInt())
  })
  // Create the serial-in, parallel-out shift register
  val zs = Reg(Vec(coeffs.length, UInt(bitWidth.W)))
  zs(0) := io.in
  for (i <- 1 until coeffs.length) {
    zs(i) := zs(i-1)
  }

  // Do the multiplies
  val products = VecInit.tabulate(coeffs.length)(i => zs(i) * coeffs(i))

  // Sum up the products
  io.out := products.reduce(_ +& _)
}
    \end{lstlisting}
    Making use of Scala features, the code here describes not just some particular
    hardware module, but more of a generator: by changing the parameters passed to
    the class FirFilter, you can change what the generated hardware does. This generator can
    become a number of different specific hardware modules:

        \begin{lstlisting}[frame=single,caption=A 3-point moving average]
new FirFilter(8, Seq(1.U, 1.U, 1.U)))
        \end{lstlisting}
        \begin{lstlisting}[frame=single,caption=A 1-cycle delay]
new FirFilter(8, Seq(0.U, 1.U)))
        \end{lstlisting}
        \begin{lstlisting}[frame=single,caption=A 5-point FIR filter with a triangle impulse response]
new FirFilter(8, Seq(1.U, 2.U, 3.U, 2.U, 1.U)))
        \end{lstlisting}
    
    While WildCAT unfortunately doesn't utilize much of these features that make
    Chisel unique to other hardware description languages, they are part of 
    the reason I wanted to become familiar with Chisel. Another part of the rationale
    is that Chisel, being released only in 2012, is fairly cutting edge for the
    industry. While not widespread, there are some companies who make use of Chisel:
    most prominently, SiFive, a fabless semiconductor company,
    makes use of Chisel for designing their RISC-V SOC designs\footnote{
      I also signed to work with them after graduation.}. 
    \section{Implementation}
    % how did I implement Wildcat?
    WildCAT could be accurately described as a straightforward, proof-of-concept
    implementation of the Wildcat ISA. It is not the most efficient or elegant
    way that WildCAT could be implemented\footnote{I discuss this in the post-mortem} ,
    but I feel it proves the "completeness" of Wildcat as an ISA, and works as a
    model to test other, theoretical implementations of the Wildcat ISA.

    Every Chisel hardware module is required to have a bundle of inputs and
    outputs -- these are required not only to have a useful piece of hardware
    \footnote{What use is hardware that can't take input or give output?}, but also to
    test your hardware. The Chisel test suite has "peek", "poke", and "expect"
    functions which can prod at a hardware module's I/O -- these could not be
    used to poke at interim values inside the hardware module itself! WildCAT's I/O
    bundle looks like this:
    \begin{lstlisting}[frame=single]
      val io = IO(new Bundle {
        val boot = Input(Bool())
        val is_write = Input(Bool())
        val write_addr = Input(UInt(24.W))
        val write_data = Input(UInt(24.W))
        val valid = Output(Bool())
        val inst = Output(UInt(24.W))
        /* debug outputs */
        val pc = Output(UInt(14.W))
        val imm = Output(UInt(14.W))
        val src = Output(UInt(24.W)) 
        val op = Output(UInt(5.W))
        val cond_pass = Output(UInt(1.W))
      }
    \end{lstlisting}
    With this, we can establish how WildCAT actually functions. In order for a CPU
    to be useful, it needs to have instructions. The way to write to WildCAT is:
    for each instruction to be written, provide the is\_write flag, the instruction
    to be written, where in ROM to put it, and ensure the boot flag is unset. Once
    a user is done writing to WildCAT, ensure is\_write is unset, set boot, and
    step the internal clock\footnote{Every hardware module has a clock by default in Chisel}.
    
    In order to test WildCAT, it needed a number of outputs. The outputs provided in
    the bundle are a hacky replacement for being able to check interim values, each
    with simple justifications:
    \begin{itemize}
      \item io.valid reports if an instruction was valid.
      \item io.inst reports the instruction at the current program counter value.
      \item io.pc reports the current program counter value.
      \item io.imm reports the value in the imm section of an instruction\footnote{For register-register
        instructions, it's actually the value at the register pointed to by the imm section.}
      \item io.src reports the value at the register pointed to by the src section of an instruction.
      \item io.op reports the opcode of the current instruction.
      \item io.cond\_pass reports the value of cond\_pass.
    \end{itemize}
    This is all good, but what does a test actually look like? Take the example
    of the Fibonacci sequence code that I wrote for the Wildcat ISA, and included
    as part of the WildCAT test suite:
    \pagebreak
    \begin{lstlisting}[frame=single,basicstyle=\scriptsize]
it should "test fibonacci sequence" in {
test(new WildcatCore()) { c =>
  write_ins(c,0.U,6189.U) // ADDI r1 0d6  
  write_ins(c,1.U,1997.U) // ADDI r30, 0d1
  write_ins(c,2.U,1070.U) // fib: SUBI r1 0x1
  write_ins(c,3.U,32704.U) // ADD r30 r31
  write_ins(c,4.U,30693.U) // MOV r31 r29
  write_ins(c,5.U,32709.U) // MOV r30 r31
  write_ins(c,6.U,31653.U) // MOV r29 r30
  write_ins(c,7.U,2092.U) // EQU r1, r0
  write_ins(c,8.U,19.U) // SKP
  write_ins(c,9.U,2066.U) // JMP fib
  write_ins(c,10.U,941.U) // ADDI r29 0d0
  boot(c)
  c.io.boot.poke(false.B)
  for(ins <- 0 until 49) {
    c.clock.step(1)
  }
  c.io.pc.expect(10.U)
  c.io.src.expect(13.U)
}
    \end{lstlisting}
    
    Breaking this down:
    \begin{itemize}
      \item write\_ins() is a helper function for writing instructions to the CPU,
      triggering the appropriate inputs.
      \item boot() is a helper function which triggers WildCAT to boot, and steps the
      clock.
      \item We unpoke the boot flag, and WildCAT begins to execute!
    \end{itemize}
    This Fibonacci sequence algorithm will find the n+1th Fibonacci number, in this
    case 13. To prove this works, we expect the program counter after the 49 cycles
    to be at address 0x10, and the value in r29 to be 13. I felt this was a good
    example of something to test WildCAT with -- even if it doesn't test every
    included feature, it demonstrates a non-trivial program which makes use of
    "branching" behavior. There are a total of 13 tests included in the WildCAT
    project, which verify each instruction specified in the Wildcat ISA.
    
    These tests, of course, are useless without a piece of hardware that can use them.
    I will not include the full source code here, but I will describe the general
    signal-flow of how WildCAT operates:
    \begin{itemize}
      \item If is\_write is set, write the given instruction at the given address in ROM.
      \item If boot is set, set the program counter to 0.
      \item Otherwise, determine if the current opcode is valid.
      \item If opcode is valid, do the action.
      \item If opcode is not valid, ignore it.
      \item Increment the program counter.
      \item Fill outputs with data.\footnote{Chisel requires all outputs to be populated
       at some point, otherwise the design will fail to compile.}
    \end{itemize}

    The details, obviously, get more complicated the further you go. For example,
    instructions come in two main "formats" -- register-immediate, and register-register.
    Register-register instructions need to operate solely within the 32
    registers that the Wildcat ISA provides, register-immediate instructions generally
    have less concerns. Implementing the decode logic such that every instruction
    would need to check that it is valid would be quite silly. Instead, I opted 
    to utilize the grouping of instructions to define the opcodes: opcodes 13 and up
    are register-immediate instructions, and opcodes below 13 are register-register.
    This simplifies the validity checking logic to a binary choice.

    In actual Chisel code, this signal flow logic roughly translates into
    a fancy looking if-then tree, with some switch statements when they made sense.
    Chisel made the individual instructions rather easy to implement -- I am fairly
    sure I spent over twice the time developing the tests for WildCAT than I did
    working on the actual implementation.
    \section{Post-Mortem}
    % how do I feel about Wildcat?
        % what was hardest?
        % how hard was it overall, compared to my expectations?
        % what would I do differently?
    One of the goals I had, which was included in my proposal, was to
    ensure WildCAT was implemented in a test-first fashion. Chisel has a built in
    testing library, which exposes functions to "poke", "peek", and "expect" values
    into whichever parts of the hardware the user wants to test. This testing
    process influenced the implementation of WildCAT immensely, from the beginning
    of development.

    In my head, I laid out a process that looked like this:
    \begin{itemize}
      \item Implement decoding logic for instructions.
      \item Implement the simple instructions (like arithmetic)
      \item Implement conditional logic.
      \item Implement the skip logic
      \item Implement a working RAM.
      \item Implement load/store instructions.
    \end{itemize}

    Like any good plans, these were destroyed within the first hour of trying to
    carry them through. I quickly realized that in order to test decoding logic
    for instructions, WildCAT needed some way to have those instructions in the
    first place. This exemplifies how I felt much of the early development was like:
    a prolonged and thorough flagellation to fully articulate what the damn thing
    would look like.
    
    Throughout the project, I felt like the actual Chisel code was... really easy.
    The lion's share of the debugging process ended up with changes to the
    WildCAT test suite, rather than WildCAT itself! I feel like that was the
    hardest part -- testing hardware is nothing like testing high-level programming
    languages. Since you cannot rely on having access to anything other than
    a hardware module's I/O, the test has to be designed to respect that. One of the
    more frustrating parts of working on WildCAT would be pouring an hour into
    debugging a test, manipulating how WildCAT itself works, just to end up
    finding out that the test methodology was wrong\footnote{Testing for a result
    before the registers actually update, for example.},
    or that the assembly code I wrote for the test was wrong. Since
    I couldn't just write some C and compile it for Wildcat, I had to write
    out the machine code by hand.\footnote{Programming calculator, actually.}
    One positive thing to come out of this is that I now
    understand how much of a pain it likely was to work with punchcards and machine language,
    and I now go to bed at night feeling blessed I live in the time where Python
    exists.
    
    I'm not particularly happy, honestly, with how the WildCAT implementation
    turned out. If I were to go back and change things, I'd probably spend some
    time breaking out the different parts of the instruction pipeline and
    figure out how to break it into different hardware submodules. It likely would 
    have been a speedier implementation, if not also easier to demonstrate in a
    visual diagram. I did, actually, try out a tool which can take Chisel
    hardware modules and create a visual flowchart for how the hardware works.
    The flowchart the tool generated for WildCAT is ginormous and fairly useless as a punchy visual
    way to explain how WildCAT works.
    
    WildCAT, in Chisel terms, is only 185 lines of code.\footnote{This translates to
    778 lines of generated Verilog, and 265 lines of FIRRTL.}
     This is largely due
    to how much Chisel gives users to build complex pieces of hardware with --
    to give hardware access to a bank of RAM, all you need to do is initialize
    a Mem() instance from the Chisel library. Registers, as well, are simple imports.
    Chisel makes it extremely easy for someone with no hardware experience
    to kick around designing hardware modules -- I can only imagine the power it
    has for a computer engineer that knows what they're doing. 
    
    The future that Chisel seems to pave the way for is a future where hardware designs can be built
    in much the same way that object-oriented software is built: breaking down
    hardware into atomic "objects" which can be arranged harmoniously
    to tackle complex tasks. I suppose, even if I'm not the happiest with WildCAT,
    the project was overall a success: I got the opportunity to play with a
    cutting-edge technology, and it got me thinking about the possibilities of
    a future that makes use of it.
      
\end{document}